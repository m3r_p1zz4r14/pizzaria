﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzaria
{
    public partial class frmMenu : Form
    {
        public frmMenu()
        {
            InitializeComponent();
            VerificarPermissoes();
        }

        void VerificarPermissoes()
        {
            if (UserSession.UsuarioLogado.PermissaoAdm == false)
            {
                if (UserSession.UsuarioLogado.PermissaoSabores == false)
                {
                    categoriaToolStripMenuItem.Enabled = false;
                }

                if (UserSession.UsuarioLogado.PermissaoPedido == false)
                {
                    pedidoToolStripMenuItem.Enabled = false;
                }
            }
        }

        void AbrirTela(UserControl tela)
        {
            if (panel2.Controls.Count > 0)
                panel2.Controls.RemoveAt(0);

            panel2.Controls.Add(tela);
        }

      

        private void novoToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            AbrirTela(new Telas.Pedido.UserControl1());
        }

       

        private void consultarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AbrirTela(new Telas.Pedido.frmPedidoConsultar());
        }

       

        private void novoToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            AbrirTela(new Telas.Categoria.frmSaboresCadastrar());
        }



        private void consultarToolStripMenuItem2_Click(object sender, EventArgs e)
        {
            AbrirTela(new Telas.Categoria.frmCategoriaConsultar());
        }


        private void label4_Click(object sender, EventArgs e)
        {
            Close();
        }

    }
}
