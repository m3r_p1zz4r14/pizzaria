﻿using Pizzaria.DB.Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Pizzaria.Telas.Cliente
{
    public partial class frmClienteNovo : Form
    {

        public ClienteDTO ClienteCriado { get; set; }

        public frmClienteNovo()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO cliente = new ClienteDTO();
                cliente.Nome = textBox6.Text.Trim();
                cliente.Cpf = maskedTextBox1.Text.Trim();
                cliente.Cep = maskedTextBox4.Text.Trim();
                cliente.Endereço = textBox3.Text.Trim();
                cliente.Telefone = maskedTextBox2.Text.Trim();

                ClienteBusiness business = new ClienteBusiness();
                business.Salvar(cliente);

                MessageBox.Show("Cliente Salvo com sucesso.", "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

        private void label6_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
