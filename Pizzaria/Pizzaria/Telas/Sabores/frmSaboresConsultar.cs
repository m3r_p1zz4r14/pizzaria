﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Pizzaria.DB.Sabores;

namespace Pizzaria.Telas.Categoria
{
    public partial class frmCategoriaConsultar : UserControl
    {
        public frmCategoriaConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                SaboresBusiness business = new SaboresBusiness();
                List<SaboresDTO> lista = business.Consultar(txtCliente.Text.Trim());

                dgvSabores.AutoGenerateColumns = false;
                dgvSabores.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
