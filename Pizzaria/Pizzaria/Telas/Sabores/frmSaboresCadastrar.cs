﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Pizzaria.DB.Sabores;

namespace Pizzaria.Telas.Categoria
{
    public partial class frmSaboresCadastrar : UserControl
    {
        public frmSaboresCadastrar()
        {
            InitializeComponent();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            try
            {
                SaboresDTO dto = new SaboresDTO();
                dto.Nome = txtnome.Text.Trim();
                dto.Preco = Convert.ToDecimal(txtvalor.Text.Trim());

                SaboresBusiness business = new SaboresBusiness();
                business.Salvar(dto);

                MessageBox.Show("Sabor salvo com sucesso.", "P1zz4r14",
                    MessageBoxButtons.OK,
                    MessageBoxIcon.Information);
                this.Hide();
        }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }
    }
}
