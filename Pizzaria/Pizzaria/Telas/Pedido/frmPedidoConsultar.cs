﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Pizzaria.DB.Pedido;

namespace Pizzaria.Telas.Pedido
{
    public partial class frmPedidoConsultar : UserControl
    {
        public frmPedidoConsultar()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                PedidoBusiness business = new PedidoBusiness();
                List<PedidoConsultarView> lista = business.Consultar(txtCliente.Text.Trim());

                dgvPedidos.AutoGenerateColumns = false;
                dgvPedidos.DataSource = lista;
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void dgvPedidos_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.ColumnIndex == 5)
                {
                    PedidoConsultarView pedido = dgvPedidos.Rows[e.RowIndex].DataBoundItem as PedidoConsultarView;

                    DialogResult r = MessageBox.Show($"Deseja realmente excluir o pedido {pedido.Id}?", "P1zz4r14",
                                        MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                    if (r == DialogResult.Yes)
                    {
                        PedidoBusiness business = new PedidoBusiness();
                        business.Remover(pedido.Id);

                        button1_Click(null, null);
                    }
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
