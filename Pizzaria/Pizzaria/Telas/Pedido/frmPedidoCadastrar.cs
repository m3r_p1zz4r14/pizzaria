﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Pizzaria.DB.Pedido;
using Pizzaria.Telas.Cliente;
using Pizzaria.DB.Cliente;
using Pizzaria.DB.Sabores;

namespace Pizzaria.Telas.Pedido
{
    public partial class UserControl1 : UserControl
    {
        BindingList<SaboresDTO> produtosCarrinho = new BindingList<SaboresDTO>();
        BindingList<ClienteDTO> ClienteAdd = new BindingList<ClienteDTO>();

        public void frmPedidoCadastrar()
        {
            InitializeComponent();
            CarregarCombos();
            ConfigurarGrid();
        }

         
        void CarregarCombos()
        {
           SaboresBusiness business = new SaboresBusiness();
            List<SaboresDTO> lista = business.Listar();

            comboBox1.ValueMember = nameof(SaboresDTO.Id);
            comboBox1.DisplayMember = nameof(SaboresDTO.Nome);
            comboBox1.DataSource = lista;
        }

        void ConfigurarGrid()
        {
            dgvItens.AutoGenerateColumns = false;
            dgvItens.DataSource = produtosCarrinho;
        }

        public UserControl1()
        {
            InitializeComponent();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            try
            {
                SaboresDTO dto = comboBox1.SelectedItem as SaboresDTO;

                int qtd = Convert.ToInt32(textBox1.Text);

                for (int i = 0; i < qtd; i++)
                {
                    produtosCarrinho.Add(dto);
                }
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreu um erro, tente mais tarde.", "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                ClienteDTO cliente = comboBox2.SelectedItem as ClienteDTO;

                PedidoDTO dto = new PedidoDTO();
                dto.FuncionarioId = UserSession.UsuarioLogado.Id;
                dto.ClienteId = cliente.Id;
                dto.FormaPagamento = comboBox3.Text.Trim();
                dto.Data = DateTime.Now;

                PedidoBusiness business = new PedidoBusiness();
                business.Salvar(dto, produtosCarrinho.ToList());

                MessageBox.Show("Pedido salvo com sucesso.", "P1zz4r14", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Hide();
            }
            catch (ArgumentException ex)
            {
                MessageBox.Show(ex.Message, "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show("Ocorreru um erro, tente mais tarde.", "P1zz4r14",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            frmClienteNovo tela = new frmClienteNovo();
            tela.ShowDialog();

            if (tela.ClienteCriado != null)
            {
                ClienteAdd.Add(tela.ClienteCriado);
                comboBox2.SelectedItem = tela.ClienteCriado;
            }
        }
    }
}
