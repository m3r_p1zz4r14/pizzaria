﻿using Pizzaria.DB.Sabores;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzaria.DB.Pedido
{
    class PedidoBusiness
    {
        public int Salvar(PedidoDTO pedido, List<SaboresDTO> produtos)
        {
            if (pedido.FormaPagamento == "Selecione")
            {
                throw new ArgumentException("Forma de Pagamento é obrigatório.");
            }

            if (pedido.ClienteId == 0)
            {
                throw new ArgumentException("Cliente é obrigatório.");
            }

            if (pedido.FuncionarioId == 0)
            {
                throw new ArgumentException("Funcionário é obrigatório.");
            }

            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            int idPedido = pedidoDatabase.Salvar(pedido);

            pedidoitembusiness itemBusiness = new pedidoitembusiness();
            foreach (SaboresDTO item in produtos)
            {
                pedidoitemDto itemDto = new pedidoitemDto();
                itemDto.IdPedido = idPedido;
                itemDto.IdSabores = item.Id;

                itemBusiness.Salvar(itemDto);
            }

            return idPedido;
        }

        public void Remover(int pedidoId)
        {
            pedidoitembusiness itemBusiness = new pedidoitembusiness();
            List<pedidoitemDto> itens = itemBusiness.ConsultarPorPedido(pedidoId);

            foreach (pedidoitemDto item in itens)
            {
                itemBusiness.Remover(item.Id);
            }

            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            pedidoDatabase.Remover(pedidoId);
        }

        public List<PedidoConsultarView> Consultar(string cliente)
        {
            PedidoDatabase pedidoDatabase = new PedidoDatabase();
            return pedidoDatabase.Consultar(cliente);
        }

    }
}
