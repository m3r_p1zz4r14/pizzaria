﻿using MySql.Data.MySqlClient;
using Pizzaria.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzaria.DB.Pedido
{
    class pedidoitemdatabase
    {
            public int Salvar(pedidoitemDto dto)
        {
            string script = @"INSERT INTO tb_pedido_item (id_sabores, id_pedido) VALUES (@id_sabores, @id_pedido)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_sabores", dto.IdSabores));
            parms.Add(new MySqlParameter("id_pedido", dto.IdPedido));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_pedido_item WHERE id_pedido_item = @id_pedido_item";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido_item", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<pedidoitemDto> ConsultarPorPedido(int idPedido)
        {
            string script = @"SELECT * FROM tb_pedido_item WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_pedido", idPedido));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<pedidoitemDto> lista = new List<pedidoitemDto>();
            while (reader.Read())
            {
                pedidoitemDto dto = new pedidoitemDto();
                dto.Id = reader.GetInt32("id_pedido_item");
                dto.IdPedido = reader.GetInt32("id_pedido");
                dto.IdSabores = reader.GetInt32("id_sabores");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
