﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzaria.DB.Pedido
{
    class pedidoitembusiness
    {
        public int Salvar(pedidoitemDto dto)
        {
            pedidoitemdatabase db = new pedidoitemdatabase();
            return db.Salvar(dto);
        }

        public void Remover(int id)
        {
            pedidoitemdatabase db = new pedidoitemdatabase();
            db.Remover(id);
        }

        public List<pedidoitemDto> ConsultarPorPedido(int idPedido)
        {
            pedidoitemdatabase db = new pedidoitemdatabase();
            return db.ConsultarPorPedido(idPedido);
        }

    }
}
