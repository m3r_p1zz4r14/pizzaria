﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzaria.Funcionario
{
    class FuncionarioDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Login { get; set; }
        public string Senha { get; set; }

        public bool PermissaoAdm { get; set; }
        public bool PermissaoSabores { get; set; }
        public bool PermissaoPedido { get; set; }
    }
}
