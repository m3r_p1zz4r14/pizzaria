﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzaria.DB.Cliente
{
    class ClienteBusiness
    {
        public int Salvar(ClienteDTO dto)
        {
            ClienteDTO cliente = this.ConsultarPorCpf(dto.Cpf);
           
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (dto.Cpf == string.Empty)
            {
                throw new ArgumentException("Cpf já cadastrado no sistema.");
            }

            if (dto.Endereço == string.Empty)
            {
                throw new ArgumentException("Endereço é obrigatório.");
            }
            if (dto.Cep == string.Empty)
            {
                throw new ArgumentException("Cep é obrigatório.");
            }

            ClienteDatabase db = new ClienteDatabase();
            return db.Salvar(dto);
        }

        public ClienteDTO ConsultarPorCpf(string cpf)
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.ConsultarPorCpf(cpf);
        }

        public List<ClienteDTO> Listar()
        {
            ClienteDatabase db = new ClienteDatabase();
            return db.Listar();
        }
    }
}
