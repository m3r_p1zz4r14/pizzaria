﻿using MySql.Data.MySqlClient;
using Pizzaria.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzaria.DB.Cliente
{
    class ClienteDatabase
    {
        public int Salvar(ClienteDTO dto)
        {
            string script = @"INSERT INTO tb_cliente (nm_cliente, ds_cpf, ds_cep, ds_endereço, ds_telefone)
                                   VALUES (@nm_cliente, @ds_cpf, @ds_cep, @ds_endereço, @ds_telefone)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_cliente", dto.Nome));
            parms.Add(new MySqlParameter("ds_cpf", dto.Cpf));
            parms.Add(new MySqlParameter("ds_cep", dto.Cep));
            parms.Add(new MySqlParameter("ds_endereço", dto.Endereço));
            parms.Add(new MySqlParameter("ds_telefone", dto.Telefone));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


        public ClienteDTO ConsultarPorCpf(string cpf)
        {
            string script = @"SELECT * FROM tb_cliente WHERE ds_cpf = @ds_cpf";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("ds_cpf", cpf));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            ClienteDTO dto = null;
            if (reader.Read())
            {
                dto = new ClienteDTO();

                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Cpf = reader.GetString("ds_cpf");
                dto.Cep = reader.GetString("ds_cep");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Endereço = reader.GetString("ds_endereço");

            }
            reader.Close();

            return dto;
        }


        public List<ClienteDTO> Listar()
        {
            string script = @"SELECT * FROM tb_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<ClienteDTO> lista = new List<ClienteDTO>();
            while (reader.Read())
            {
                ClienteDTO dto = new ClienteDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_cliente");
                dto.Cpf = reader.GetString("ds_cpf");
                dto.Cep = reader.GetString("ds_cep");
                dto.Telefone = reader.GetString("ds_telefone");
                dto.Endereço = reader.GetString("ds_endereço");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
