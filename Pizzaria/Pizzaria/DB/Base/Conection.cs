﻿using MySql.Data.MySqlClient;
using Pizzaria.DB.Base;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzaria.DB.Base
{
    class Conection
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost;database=PizzariaDB;uid=root;password=1234;sslmode=none";

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            return connection;
        }
    }
}
