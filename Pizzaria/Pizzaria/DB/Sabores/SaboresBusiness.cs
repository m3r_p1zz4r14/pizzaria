﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzaria.DB.Sabores
{
    class SaboresBusiness
    {
        public int Salvar(SaboresDTO dto)
        {
            if (dto.Nome == string.Empty)
            {
                throw new ArgumentException("Nome é obrigatório.");
            }

            if (dto.Preco <= 0)
            {
                throw new ArgumentException("Preço é obrigatório.");
            }

            SaboresDatabase db = new SaboresDatabase();

            List<SaboresDTO> produtos = db.Consultar(dto.Nome);
            if (produtos.Count > 0)
            {
                throw new ArgumentException("Sabor com o mesmo nome já cadastrado.");
            }

            return db.Salvar(dto);
        }


        public void Alterar(SaboresDTO dto)
        {
            SaboresDatabase db = new SaboresDatabase();
            db.Alterar(dto);
        }


        public void Remover(int id)
        {
            SaboresDatabase db = new SaboresDatabase();
            db.Remover(id);
        }


        public List<SaboresDTO> Consultar(string nome)
        {
            SaboresDatabase db = new SaboresDatabase();
            return db.Consultar(nome);
        }


        public List<SaboresDTO> Listar()
        {
            SaboresDatabase db = new SaboresDatabase();
            return db.Listar();
        }
    }
}
