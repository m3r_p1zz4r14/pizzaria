﻿using MySql.Data.MySqlClient;
using Pizzaria.DB.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Pizzaria.DB.Sabores
{
    class SaboresDatabase
    {
        public int Salvar(SaboresDTO dto)
        {
            string script = @"INSERT INTO tb_sabores (nm_sabor, vl_preco) 
                                   VALUES (@nm_sabor, @vl_preco)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_sabor", dto.Nome));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));

            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(SaboresDTO dto)
        {
            string script = @"UPDATE tb_sabores
                                 SET nm_sabor  = @nm_sabor,
                                     vl_preco    = @vl_preco,
                               WHERE id_sabores = @id_sabores";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_sabores", dto.Id));
            parms.Add(new MySqlParameter("nm_sabor", dto.Nome));
            parms.Add(new MySqlParameter("vl_preco", dto.Preco));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_sabores WHERE id_sabores = @id_sabores";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_sabores", id));

            Database db = new Database();
            db.ExecuteInsertScript(script, parms);
        }

        public List<SaboresDTO> Consultar(string sabores)
        {
            string script = @"SELECT * FROM tb_sabores WHERE nm_sabor like @nm_sabor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_sabor", sabores + "%"));

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<SaboresDTO> lista = new List<SaboresDTO>();
            while (reader.Read())
            {
                SaboresDTO dto = new SaboresDTO();
                dto.Id = reader.GetInt32("id_sabores");
                dto.Nome = reader.GetString("nm_sabor");
                dto.Preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<SaboresDTO> Listar()
        {
            string script = @"SELECT * FROM tb_sabores";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<SaboresDTO> lista = new List<SaboresDTO>();
            while (reader.Read())
            {
                SaboresDTO dto = new SaboresDTO();
                dto.Id = reader.GetInt32("id_sabores");
                dto.Nome = reader.GetString("nm_sabor");
                dto.Preco = reader.GetDecimal("vl_preco");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
